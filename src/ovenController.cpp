#include "Arduino.h"

#define OVEN_TEMP_SENSOR 0
#define REF_TEMP_SENSOR  1
#define PWM_OUTPUT       2

#define GAIN             2.7
#define NOMINAL_TEMP_PW  17

// Uncomment following line to "fix" the code and then rebuild project.
//#define FIXED

unsigned char calculate_new_oven_ON_time(unsigned char actual_temp, unsigned char ref_temp) __attribute__((noinline));

void setup() {
    // Initialise all hardware
    // - Pin 13 LED to blink "keep-alive LED"
    // - A0 as oven temperature sensor
    // - A1 as reference temperate sensor
    // - 2 as PWM output - initially "0"
    
    pinMode(13, OUTPUT);
    
    digitalWrite(13, LOW);
    
    analogRead(OVEN_TEMP_SENSOR);
    
    analogRead(REF_TEMP_SENSOR);
    
    analogWrite(PWM_OUTPUT, 0);

}

void loop() {
    int ovenTemp = 0;
    int refTemp = 0;
    
    unsigned char ucOvenTemp = 0;
    unsigned char ucRefTemp = 0;
    
    unsigned char ovenPwmOutput = 0;
    
    // Read the temperatures
    ovenTemp = analogRead(OVEN_TEMP_SENSOR);
    refTemp = analogRead(REF_TEMP_SENSOR);
    
    // Convert values to byte values
    ucOvenTemp = (unsigned char)(ovenTemp / 4);
    ucRefTemp = (unsigned char)(refTemp / 4);
    
    // Calculate required PWM value
    ovenPwmOutput = calculate_new_oven_ON_time(ucOvenTemp, ucRefTemp);
    
    // Set new heater control PWM value
    analogWrite(PWM_OUTPUT, ovenPwmOutput);
    
    // Show we're alive
    digitalWrite(13, LOW);
    
    // Wait for 500ms
    delay(250);
    
    // Show we're alive
    digitalWrite(13, HIGH);
    
    // Wait for 500ms
    delay(250);
}

unsigned char calculate_new_oven_ON_time(unsigned char actual_temp, unsigned char ref_temp)
{
  signed char delta_temp;
  unsigned char newPwmValue;
//  signed char newPwmValue;

  delta_temp = actual_temp - ref_temp;

  newPwmValue = NOMINAL_TEMP_PW + (GAIN * delta_temp);

  if (newPwmValue > 100)
  newPwmValue = 100;

  if (newPwmValue < 0)
  newPwmValue = 0;

  return (unsigned char)newPwmValue;
}

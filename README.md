# Oven Controller Project #

This project has been created to support the BugHunter blog entry "Building test into your development process" which can be found at the [BugHunter Blog](http://bit.ly/bughunterblog).
The purpose of this project is to show how Original Binary Code (OBC) testing can be integrated into the embedded software development process to find potential bugs before the code is released.
The project makes use of the free IDE winIDEA Open and the testing environment testIDEA, which is included.

Thanks go to Lisa Simone for the idea behind this project which is the subject of her book [If I Only Changed the Software, Why Is the Phone on Fire?](http://bit.ly/if-I-only-changed-the-software)

## What will I need? ##

In order to use this project you will need:

* [winIDEA Open Version 9.12.256](http://bit.ly/winideaopen)
* [Arduino M0 Pro](http://bit.ly/arduino-m0-pro) Board

Optionally you can also install:

* [Arduino IDE](http://bit.ly/arduino-org) from arduino.org if you wish to build the project yourself
* [MinGW](http://bit.ly/mingw-download) if you wish to build the project yourself
* [git for Windows](http://bit.ly/windows-git) if you want to clone this repository (or your preferred git package)

## How do I use this code? ##

There is a little more detail in the blog post. A short overview is provided here:

1. Download and install the software needed.
2. Clone this repository or download the whole thing using the link on the left.
3. Plug your Arduino M0 Pro into your PC via a suitable USB cable.
4. Open the "ovenController256.xjrf" workspace in winIDEA Open.
5. From the menu, under Debug -> Files For Download... select the version of code you wish to program into your Arduino M0 Pro (either the "sketch-original.elf" which will fail testing in testIDEA, or "sketch-fixed.elf" which will pass).
6. From the menu, select Test -> Launch testIDEA. If prompted, open the "Oven Controller.iyaml" file.
7. From the testIDEA menu, select Test -> Run All Tests

## Is xxx supported? ##

Currently this project only works with the M0 Pro as it is fitted with the Atmel (Microchip) EDBG debugger on the programming port.
This project will not work with the Arduino M0. It could work with the Arduino/Genuino Zero - if you would like this, please let me know.

### Contribution guidelines ###

If you wish to get in contact with us or contribute, please drop me an email to stuart dot cording at isystem dot com